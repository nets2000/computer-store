import Text from "./BaseUIObjects/Text.js";
import Image from "./BaseUIObjects/Image.js";
import MoneyManager from "./Managers/MoneyManager.js";
import ComputerManager from "./Managers/ComputerManager.js";

// All vars for positions in the project
const xSizeBoxes = 250;
const ySizeBoxes = 300;
const startBoxYPos = 150;
const startBoxXPos = 100;
const spaceBetweenBoxes = 350;
const offsetInBox = 20;
let addedXPosBetweenBoxes = 0;

// Create title banner for website
new Image(0, 0, './Images/TitleBackground.png', spaceBetweenBoxes * 3 + startBoxXPos, 80);
new Text('div', startBoxXPos, 12.5, "Komputer Store", 40, 'left', 300, '', 'white');

// Create three background boxes with the titles
const titlesBoxes = ["Joe Banker", "Work", "Laptops"];
for (let i = 0; i < titlesBoxes.length; i++) {
    new Image(startBoxXPos + addedXPosBetweenBoxes, startBoxYPos, './Images/BackgroundBoxes.png', xSizeBoxes, ySizeBoxes);
    let newTitle = new Text('div', startBoxXPos + offsetInBox + addedXPosBetweenBoxes, startBoxYPos + offsetInBox, titlesBoxes[i], 35, 'left', xSizeBoxes - 2 * offsetInBox, 
    'bold');

    addedXPosBetweenBoxes += spaceBetweenBoxes;
}

// Create large under box where computers can be bought
const startYPosLargeBox = 500;
new Image(startBoxXPos - 5, startYPosLargeBox, './Images/BackgroundBoxes.png', addedXPosBetweenBoxes - startBoxXPos + 5, ySizeBoxes);

// Create extra vars for button sizes and posses
const buttonYSize = 50;
const buttonYPos = startBoxYPos + ySizeBoxes - offsetInBox - buttonYSize;
const xSizeSmallButton = xSizeBoxes / 2 - offsetInBox * 1.5;

// Instantiate both managers with their required vars so they can set all posses correctly of the ui elements
let moneyManager = new MoneyManager(xSizeBoxes, startBoxYPos ,startBoxXPos, offsetInBox, buttonYSize, buttonYPos, xSizeSmallButton, spaceBetweenBoxes);
let computerManager = new ComputerManager(xSizeBoxes, ySizeBoxes, startBoxYPos, startBoxXPos, spaceBetweenBoxes, offsetInBox, addedXPosBetweenBoxes, buttonYSize, 
    startYPosLargeBox);