import ComputerManager from "../Managers/ComputerManager.js";
import Dropdown from "../BaseUIObjects/Dropdown.js";

// Extend from the dropdown class
export default class SelectComputerDropdown extends Dropdown {
    // Get dropdown object from the computer manager and change data based on selected index of the dropdown
    OnOptionChangeEvent() {
        const dropdown = ComputerManager.instance.selectComputerDropdown.uiObj;
        ComputerManager.instance.ChangeDescriptionOfData(dropdown.options[dropdown.selectedIndex].value);
    }
}