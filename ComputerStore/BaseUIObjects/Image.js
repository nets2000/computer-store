import BaseUIObj from "./BaseUIObj.js";

// Extend from the BaseUIObj class
export default class Image extends BaseUIObj {
    /*
    xPos = x position on screen
    yPos = y position on screen
    imgSrc = source of the image
    imgWidth = width of the dropdown
    imgHeight = height of the dropdown
    color = color of the text in the dropdown
    display = visible state of object

    Create an image
    */
    constructor(xPos, yPos, imgSrc, imgWidth, imgHeight, color = 'white', display = "block") {
        super('img', xPos, yPos, color, display);
        
        this.uiObj.src = imgSrc;
        this.uiObj.style.width = imgWidth + 'px'; 
        this.uiObj.style.height = imgHeight + 'px';
    }

    // Set a new image source
    SetNewImage(imgSrc) {
        this.uiObj.src = imgSrc;
    }
}