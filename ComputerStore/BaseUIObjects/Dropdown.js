import BaseUIObj from "./BaseUIObj.js";

// Extend from the BaseUIObj class
export default class Dropdown extends BaseUIObj {
    /*
    xPos = x position on screen
    yPos = y position on screen
    dropDownWidth = width of the dropdown
    dropDownHeight = height of the dropdown
    dropDownElements = all elements added to the dropdown
    fontsize = fontsize of text in dropdown
    dropdownColor = color of the dropdown
    color = color of the text in the dropdown
    display = visible state of object

    Create dropdown and set dropdown option select event to listener
    */

    constructor(xPos, yPos, dropDownWidth, dropDownHeight, dropDownElements, fontSize, dropdownColor = 'white', color = 'black', display = "block") {
        super('select', xPos, yPos, color, display);
        
        this.uiObj.style.backgroundColor = dropdownColor;
        this.uiObj.style.width = dropDownWidth + 'px'; 
        this.uiObj.style.height = dropDownHeight + 'px';
        this.uiObj.style.fontSize =  fontSize + 'px';

        for (let i = 0; i < dropDownElements.length; i++) {
            let option = document.createElement("option");
            option.style.fontSize = fontSize + 'px';
            option.value = i;
            option.text = dropDownElements[i];
            this.uiObj.appendChild(option);
        }

        this.uiObj.addEventListener("change", this.OnOptionChangeEvent); 
    }

    // The function that triggers when an option is changed
    OnOptionChangeEvent() {
        console.log("Option changed");
    }
}