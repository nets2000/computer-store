import BaseUIObj from "./BaseUIObj.js";

// Extend from the BaseUIObj class
export default class Text extends BaseUIObj {
    /*
    elementType = type of html element
    xPos = x position on screen
    yPos = y position on screen
    text = text displayed
    fontSize = fontsize of text
    width = width of text element
    fontWeight = font style used for text
    color = color of the text
    display = visible state of object

    Create an text element
    */
    constructor(elementType ,xPos, yPos, text, fontSize, textAlign = 'center', width = 100, fontWeight = '', color = 'black', display = "block") {
        super(elementType, xPos, yPos, color, display);
        this.uiObj.innerHTML = text;
        this.uiObj.style.fontSize =  fontSize + 'px';
        this.uiObj.style.textAlign = textAlign;
        if (fontWeight != '') {
            this.uiObj.style.fontWeight = fontWeight;
        }

        this.uiObj.style.width = width + 'px'; 
    }

    // Change text of the element
    EditText(newText) {
        this.uiObj.innerHTML = newText;
    }

    // Change fontsize of the element
    EditFontSize(newFontSize) {
        this.uiObj.style.fontSize = newFontSize + 'px';
    }
}