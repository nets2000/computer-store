// A base template class for all ui objects
export default class BaseUIObj {
    /*
    elementType = type of html object
    xPos = x position on screen
    yPos = y position on screen
    color = color of the object
    display = visible state of object

    Create and append the new html object to the program
    */

    constructor(elementType, xPos, yPos, color = 'black', display = "block") {
        this.uiObj = document.createElement(elementType);
        this.uiObj.style.position = 'absolute';
        
        this.uiObj.style.top = yPos + 'px';
        this.uiObj.style.left = xPos + 'px';
        
        this.uiObj.style.color = color;
        this.uiObj.style.display = display;
        
        document.body.appendChild(this.uiObj);
    }

    // Edit the visibility state of the object
    EditUIVisibility(visible) {
        if (visible) {
            this.uiObj.style.display = "block";
        } else {
            this.uiObj.style.display = "none";
        }
    }
}