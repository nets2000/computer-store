import Text from "./Text.js";

// Extend from the text class
export default class Button extends Text {
    /*
    xPos = x position on screen
    yPos = y position on screen
    buttonWidth = width of the button
    buttonHeight = height of the button
    text = text written in button
    fontsize = fontsize of text in button
    buttonColor = color of the button
    fontWeight = type of font in button
    color = color of the text in the button
    display = visible state of object

    Create button and set button click event to listener
    */
    
    constructor(xPos, yPos, buttonWidth, buttonHeight, text, fontSize, buttonColor = 'white', fontWeight = '', color = 'black', display = "block") {
        super('button', xPos, yPos, text, fontSize, 'center', buttonWidth, fontWeight, color, display);
        
        this.uiObj.style.backgroundColor = buttonColor;
        this.uiObj.style.width = buttonWidth + 'px'; 
        this.uiObj.style.height = buttonHeight + 'px';
        this.uiObj.addEventListener("click", this.ButtonClickEvent); 
    }

    // Event that happens on button click
    ButtonClickEvent() {
        console.log("Button clicked");
    }
}