import MoneyManager from "../Managers/MoneyManager.js";
import Button from "../BaseUIObjects/Button.js";

// Extend from the button class
export default class WorkButton extends Button {
    // Add 100 money to the pay amount with add pay function from money manager
    ButtonClickEvent() {
        MoneyManager.instance.AddPayAmount(100);
    }
}