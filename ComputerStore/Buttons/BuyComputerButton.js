import ComputerManager from "../Managers/ComputerManager.js";
import Button from "../BaseUIObjects/Button.js";

// Extend from the button class
export default class BuyComputerButton extends Button {
    // Call the buy computer function
    ButtonClickEvent() {
        ComputerManager.instance.BuyComputer();
    }
}