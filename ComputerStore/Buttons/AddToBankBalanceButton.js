import MoneyManager from "../Managers/MoneyManager.js";
import Button from "../BaseUIObjects/Button.js";

// Extend from the button class
export default class AddToBankBalanceButton extends Button {
    // Call the switch pay to bank balance function
    ButtonClickEvent() {
        MoneyManager.instance.SwitchPayToBankBalance();
    }
}