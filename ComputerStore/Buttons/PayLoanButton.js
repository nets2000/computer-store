import MoneyManager from "../Managers/MoneyManager.js";
import Button from "../BaseUIObjects/Button.js";

// Extend from the button class
export default class PayLoanButton extends Button {
    // override click with the pay loan function from money manager
    ButtonClickEvent() {
        MoneyManager.instance.PayLoan();
    }
}