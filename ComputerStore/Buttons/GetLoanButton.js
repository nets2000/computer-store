import MoneyManager from "../Managers/MoneyManager.js";
import Button from "../BaseUIObjects/Button.js";

// Extend from the button class
export default class GetLoanButton extends Button {
    // Edit button click event to show a prompt where the user can fill in any value
    ButtonClickEvent() {
        const addedMoneyFromLoan = parseFloat(prompt("How much money is the loan?"));

        // Make sure sure filled in a number
        if (!Number.isNaN(addedMoneyFromLoan)) {
            // Loan needs to more then  0
            if (addedMoneyFromLoan > 0) {
                // The max money lend from the loan is twice the amount of money is the users bank account
                if (addedMoneyFromLoan <= MoneyManager.instance.bankBalance * 2) {
                    // Add loan to to bank/ loan account
                    MoneyManager.instance.AddLoanAmount(addedMoneyFromLoan);
                    MoneyManager.instance.AddBankBalance(addedMoneyFromLoan);
                } else {
    // Handle incorrect use cases with alerts
                    alert("The loan has to be lower then or equal to double the bank accounts money.");
                }
            } else {
                alert("The loan can't be negative or 0.");
            }
        } else {
            alert("Please fill in a number.");
        }

        // Disable loan button if the loan is higher then 0
        if (MoneyManager.instance.loanAmount > 0) {
            MoneyManager.instance.getLoanButton.EditUIVisibility(false);
        }

    }
}