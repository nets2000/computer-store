import AddToBankBalanceButton from "../Buttons/AddToBankBalanceButton.js";
import GetLoanButton from "../Buttons/GetLoanButton.js";
import PayLoanButton from "../Buttons/PayLoanButton.js";
import WorkButton from "../Buttons/WorkButton.js";
import Singleton from "../singleton.js";
import Text from "../BaseUIObjects/Text.js";

// Extend from the singleton class
export default class MoneyManager extends Singleton {
    constructor(xSizeBoxes, startBoxYPos, startBoxXPos, offsetInBox, buttonYSize, buttonYPos, xSizeSmallButton, spaceBetweenBoxes) {
        super();
        // Create the money vars
        this.loanAmount = 0;
        this.currentPay = 0;
        this.bankBalance = 0;

        // Create all button required for handling money
        this.getLoanButton = new GetLoanButton(startBoxXPos + offsetInBox, buttonYPos, xSizeBoxes - 2 * offsetInBox, buttonYSize, 'Get a loan', 20, 'aquamarine');
        this.bankButton = new AddToBankBalanceButton(startBoxXPos + spaceBetweenBoxes + offsetInBox, buttonYPos, xSizeSmallButton, buttonYSize, 'Bank', 20, 'yellow');
        this.workButton = new WorkButton(startBoxXPos + spaceBetweenBoxes + offsetInBox * 2 + xSizeSmallButton, buttonYPos, xSizeSmallButton, buttonYSize, 'Work', 20, 'blue');
        this.payLoanButton = new PayLoanButton(startBoxXPos + spaceBetweenBoxes + offsetInBox, buttonYPos - buttonYSize - offsetInBox, xSizeBoxes - 2 * offsetInBox, buttonYSize, 
        'Repay Loan', 20, 'red', '', 'black', 'none');

        // Create text to show all balances of the money
        let balanceText = new Text('div', startBoxXPos + offsetInBox, startBoxYPos + offsetInBox + 50, 'Balance', 20, 'left', xSizeBoxes / 2);
        this.outstandingLoanText = new Text('div', startBoxXPos + offsetInBox, startBoxYPos + offsetInBox + 75, 'Loan', 20, 'left', xSizeBoxes / 2, '', 'black', 'none');
        this.amountOfMoneyText = new Text('div', startBoxXPos + offsetInBox, startBoxYPos + offsetInBox + 50, '0 Kr', 20, 'right', xSizeBoxes - offsetInBox * 2);
        this.amountOfLoanText = new Text('div', startBoxXPos + offsetInBox, startBoxYPos + offsetInBox + 75, '0 Kr', 20, 'right', xSizeBoxes - offsetInBox * 2, 
        '', 'black', 'none');

        let payText = new Text('div', startBoxXPos + offsetInBox + spaceBetweenBoxes, startBoxYPos + offsetInBox + 50, 'Pay', 20, 'left', xSizeBoxes / 2);
        this.amountOfPayText = new Text('div', startBoxXPos + offsetInBox + spaceBetweenBoxes, startBoxYPos + offsetInBox + 50, '0 Kr', 20, 'right', xSizeBoxes - offsetInBox * 2);
    }

    // Edit the pay amount and update the text
    AddPayAmount(changedAmount) {
        this.currentPay += changedAmount;
        this.amountOfPayText.EditText(`${this.currentPay} Kr`);
    }

    // Edit the loan amount and update the text
    AddLoanAmount(changedAmount) {
        this.loanAmount += changedAmount;
        this.amountOfLoanText.EditText(`${this.loanAmount} Kr`);

        // The loan UI is visible based on whether there is a loan or not
        if (this.loanAmount > 0) {
            this.ChangeLoanVisibility(true);
        } else {
            this.ChangeLoanVisibility(false);
        }
    }

    // Change the visibility of all the loan UI
    ChangeLoanVisibility(visible) {
        this.payLoanButton.EditUIVisibility(visible);
        this.outstandingLoanText.EditUIVisibility(visible);
        this.amountOfLoanText.EditUIVisibility(visible);
        // The visibility of the get loan button is opposite
        this.getLoanButton.EditUIVisibility(!visible);
    }

    // Edit bank balance and update text
    AddBankBalance(changedAmount) {
        this.bankBalance += changedAmount;
        this.amountOfMoneyText.EditText(`${this.bankBalance} Kr`);      
    }

    // Sets pay value to the bank balance value, detracts from pay if there is a loan
    SwitchPayToBankBalance() {
        // Check for loan
        if (this.loanAmount > 0) {
            // 10% of pay goes to loan, 90% to bank
            let payToLoan = this.currentPay * 0.1;
            let payToBank = this.currentPay * 0.9;
            
            // If the pay to loan is bigger then loan add leftover money to pay to bank
            if (payToLoan > this.loanAmount) {
                payToBank += payToLoan - this.loanAmount;
                payToLoan = this.loanAmount;
            }

            // Update balances
            this.AddBankBalance(payToBank);
            this.AddLoanAmount(-payToLoan);
        } else{
            // Add pay to bank if there is no loan
            this.AddBankBalance(this.currentPay);
        }

        // Detract all pay
        this.AddPayAmount(-this.currentPay);
    }


    PayLoan() {
        // Check if pay is bigger then loan
        if (this.currentPay > this.loanAmount) {
            // Detract all pay from loan and add leftover pay to bank if pay is bigger
            let addedToBank = this.currentPay - this.loanAmount;
            this.AddBankBalance(addedToBank);
            this.AddLoanAmount(-this.loanAmount);
        } else {
            // Detract all pay from loan if loan is bigger or equal
            this.AddLoanAmount(-this.currentPay);
        }

        // Detract all pay
        this.AddPayAmount(-this.currentPay);
    }
}