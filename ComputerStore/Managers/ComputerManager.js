import BuyComputerButton from "../Buttons/BuyComputerButton.js";
import SelectComputerDropdown from "../Dropdowns/SelectComputerDropdown.js";
import Singleton from "../singleton.js";
import Image from "../BaseUIObjects/Image.js";
import Text from "../BaseUIObjects/Text.js";
import MoneyManager from "./MoneyManager.js";

// Extend from the singleton class
export default class ComputerManager extends Singleton {
    constructor(xSizeBoxes, ySizeBoxes, startBoxYPos, startBoxXPos, spaceBetweenBoxes, offsetInBox, addedXPosBetweenBoxes, buttonYSize, startYPosLargeBox) {
        super();
        this.currentIndexComputer = 0;

        this.GetComputerInformation(xSizeBoxes, ySizeBoxes, startBoxYPos, startBoxXPos, spaceBetweenBoxes, offsetInBox, addedXPosBetweenBoxes, buttonYSize, startYPosLargeBox);
    }

    // Fetch data from the API
    async GetComputerInformation(xSizeBoxes, ySizeBoxes, startBoxYPos, startBoxXPos, spaceBetweenBoxes, offsetInBox, addedXPosBetweenBoxes, buttonYSize, startYPosLargeBox) {
        try {
            this.computerData = [];

            await fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
            .then(response => response.json())
            .then(data => this.computerData = data);

            // When data is collected and stored, create ui
            this.CreateBuyComputerFunctions(xSizeBoxes, ySizeBoxes, startBoxYPos, startBoxXPos, spaceBetweenBoxes, offsetInBox, addedXPosBetweenBoxes, buttonYSize, 
            startYPosLargeBox);
        } catch (error) {
            console.error(error);
        }
    }

    // Create all computer buy functions UI
    CreateBuyComputerFunctions(xSizeBoxes, ySizeBoxes, startBoxYPos, startBoxXPos, spaceBetweenBoxes, offsetInBox, addedXPosBetweenBoxes, buttonYSize, startYPosLargeBox) {
        // Create an array of names of the computer names for the dropdown
        let computerNames = [];
        for (let i = 0; i < this.computerData.length; i++) {
            computerNames.push(this.computerData[i].title);
        }
        
        // Create dropdown, feature bold text and the text of the features that all computers have
        this.selectComputerDropdown = new SelectComputerDropdown(startBoxXPos + offsetInBox + spaceBetweenBoxes * 2, startBoxYPos + offsetInBox + 50, 
        xSizeBoxes - 2 * offsetInBox, 35, computerNames, 20);
        let featureTitleText = new Text('div', startBoxXPos + offsetInBox + spaceBetweenBoxes * 2, startBoxYPos + offsetInBox + 105, 'Features:', 20, 'left', 
        xSizeBoxes - offsetInBox * 2, 'bold');
        this.featuresText = new Text('div', startBoxXPos + offsetInBox + spaceBetweenBoxes * 2, startBoxYPos + offsetInBox + 127.5, 'Hi<br>Hi<br>Hi<br>Hi', 17, 'left', 
        xSizeBoxes - offsetInBox * 2);
        
        // Vars for putting objects in the correct place
        const yOffsetOfLargeBox = 50;
        const xOffsetOfLargeBox = yOffsetOfLargeBox / 1.5;
        const sizeComputerImage = ySizeBoxes - 2 * yOffsetOfLargeBox;
        const xSizeOfLargeBox = addedXPosBetweenBoxes - startBoxXPos;
        const sizeOfLastPartOfBigBox = xSizeOfLargeBox - (xOffsetOfLargeBox * 2 + sizeComputerImage + xSizeOfLargeBox / 2.5);
        
        // Create all the UI for in the big box where the user can buy a computer
        this.computerImage = new Image(startBoxXPos + xOffsetOfLargeBox, startYPosLargeBox + yOffsetOfLargeBox, './Images/TitleBackground.png', sizeComputerImage, 
        sizeComputerImage);
        this.computerTitle = new Text('div', startBoxXPos + xOffsetOfLargeBox * 2 + sizeComputerImage, startYPosLargeBox + yOffsetOfLargeBox - 12.5, 'Insane Computer', 50, 
        'left', xSizeOfLargeBox / 2.5, 'bold');
        this.computerDescription = new Text('div', startBoxXPos + xOffsetOfLargeBox * 2 + sizeComputerImage, startYPosLargeBox + yOffsetOfLargeBox + 55, 'Insane description', 20, 'left', 
        xSizeOfLargeBox / 2.5);
        this.computerPriceText = new Text('div', startBoxXPos + xOffsetOfLargeBox * 2 + sizeComputerImage + xSizeOfLargeBox / 2.5, startYPosLargeBox + yOffsetOfLargeBox, 
        '1500 NOK', 40, 'center', sizeOfLastPartOfBigBox, 'bold');
        let buyComputerButton = new BuyComputerButton(startBoxXPos + xOffsetOfLargeBox * 2 + sizeComputerImage + xSizeOfLargeBox / 2.5 + sizeOfLastPartOfBigBox / 2 - 
        (xSizeBoxes - 2 * offsetInBox) / 2, startYPosLargeBox + yOffsetOfLargeBox + 100, xSizeBoxes - 2 * offsetInBox, buttonYSize, 'BUY NOW', 20, 'lightSkyBlue', 'bold');
        this.computerStockLeftText = new Text('div', startBoxXPos + xOffsetOfLargeBox * 2 + sizeComputerImage + xSizeOfLargeBox / 2.5, 
        startYPosLargeBox + yOffsetOfLargeBox + 155, 'Stock: 1 left', 20, 'center', sizeOfLastPartOfBigBox);
        
        // Set data to first cpu
        this.ChangeDescriptionOfData();
    }

    // Set descriptions of UI to the descriptions gotten from the API based on index
    ChangeDescriptionOfData(index = "0") {
        let featuresText = "";
        for (let i = 0; i < this.computerData[index].specs.length; i++) {
            featuresText += this.computerData[index].specs[i] + "<br>";
        }
        this.featuresText.EditText(featuresText);

        // Change source from jpeg to png for The Visor computer
        if (index == "4") {
            let src = `https://noroff-komputer-store-api.herokuapp.com/${this.computerData[index].image}`;
            src = src.slice(0, src.length - 3);
            src += "png";
            this.computerImage.SetNewImage(src);
        } else {
        // The rest of the computers don't have broken links
            this.computerImage.SetNewImage(`https://noroff-komputer-store-api.herokuapp.com/${this.computerData[index].image}`);
        }

        this.computerTitle.EditText(this.computerData[index].title);
        this.computerDescription.EditText(this.computerData[index].description);
        this.computerPriceText.EditText(`${this.computerData[index].price} NOK`);
        this.computerStockLeftText.EditText(`Stock: ${this.computerData[index].stock} left`);

        // Some descriptions were to large
        // TODO: Auto size for text
        switch(index) {
            case "2":
                this.ChangeFontSizeOfComputerTitleAndFeatures(38, 14);
                break;
            case "5":
                this.ChangeFontSizeOfComputerTitleAndFeatures(50, 16);
                break;
            default:
                this.ChangeFontSizeOfComputerTitleAndFeatures(50, 17);
                break;
        }

        this.currentIndexComputer = index;
    }

    // Resize the fontsize of the computer title/ feature list
    ChangeFontSizeOfComputerTitleAndFeatures(computerTitleFontSize, featureTextFontSize) {
        this.computerTitle.EditFontSize(computerTitleFontSize);
        this.featuresText.EditFontSize(featureTextFontSize);
    }

    // Use the currently selected index the get the correct computer data.
    // Use this data to check if the computer is still in stock and to check whether you have enough money
    BuyComputer() {
        let requiredData = this.computerData[this.currentIndexComputer];
        // Price check
        if (MoneyManager.instance.bankBalance < requiredData.price) {
            alert(`You don't have enough money to buy the ${requiredData.title}.`);
        // Stock check
        } else if (requiredData.stock <= 0) {
            alert(`The ${requiredData.title} is out of stock. Come back later!`);
        // Buy and detract money/ stock
        } else {
            requiredData.stock--;
            this.computerStockLeftText.EditText(`Stock: ${requiredData.stock} left`);
            MoneyManager.instance.AddBankBalance(-requiredData.price);
            alert(`You now own this beautiful new ${requiredData.title}!`);
        }
    }
}